# Scala

scala是一个混合面向对象特性和函数式编程特性的语言。

spark使用scala编写。

## scala基本语法与语言特性

### 与java的不同之处

Scala的import没有`;`。导入包中的全部内容不使用`*`,而是使用`_`，
因为`*`在scala中是一个合法的标识符字符。导入一个包中的几个类时不需要
重复写多行相同前缀的import语句，使用`import java.util.{Date, Locale}`。

变量声明使用`val now = new Date`

封装代码的东西叫做`object`，同时也有`class`，暂时不清楚区别

定义一个函数形式如下:

```scala
def main(args: Array[String]) {
	// ...
}
```

函数调用的形式可以用通常的`obj.method(args ...)`的形式。但是在scala中
如果函数接受一个参数，也可以写成中序表达式(infix expression)的形式：
`obj method arg`。

// 未完待续

## scala与java的互操作

// 未完待续
