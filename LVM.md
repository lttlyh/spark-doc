# LVM

## 磁盘操作相关命令汇总

* fdisk
* gdisk
* parted
* partprobe - 刷新磁盘分区表 （inform the OS of partition table changes）
* lsblk / blkid / findmnt
* mount / unmount
* mkfs.xxx
* fsck.xxx
* [Loop Device]
  * losetup
  * mount -o loop

## LVM 概念

LVM用来将多个分区合并成一个称为卷组(volume group)的整体，类似一个虚拟的硬盘，
并且可以从这个卷组中分配出多个逻辑卷(logical volume)，类似分区。

* PV(Physical Volume) - 拿来放到lvm卷组中去的一个物理分区。
* VG(Volume Group) - 一个卷组可以添加多个物理卷来存储内容，相当于把多个物理卷合并成一个虚拟硬盘
* LV(Logical Volume) - 一个卷组中可以创建多个逻辑卷
* PE(Physical Extend) - 唯一编号的存储单元，该存储单元在物理卷中
* LE(Logical Extend) - 逻辑卷用到的存储单元，对应于一个PE

## 好处

* 容量可以自定义，可以通过添加物理卷来扩容一个卷组，不知道可不可以从一个卷组中释放一个物理卷。
一个逻辑卷的空间也可以通过命令来增加和减少。简单来说，虚拟硬盘（VG）的空间，虚拟分区（LV）
的空间都可以动态分配而不丢失数据。其中因为只要是一个物理分区都可以添加到VG中，不管来自哪个硬盘，
所以一个VG的大小可以突破一个硬盘空间大小的限制。
* 不清楚了。。。

## LVM2 包自带的命令

真尼玛多 :(

* blkdeactivate
* fsadm
* lvchange
* lvconvert
* lvcreate
* lvdisplay
* lvextend
* lvm
* lvmchange
* lvmconf
* lvmdiskscan
* lvmdump
* lvmetad
* lvmsadc
* lvmsar
* lvreduce
* lvremove
* lvrename
* lvresize
* lvs
* lvscan
* pvchange
* pvck
* pvcreate
* pvdisplay
* pvmove
* pvremove
* pvresize
* pvs
* pvscan
* vgcfgbackup
* vgcfgrestore
* vgchange
* vgck
* vgconvert
* vgcreate
* vgdisplay
* vgexport
* vgextend
* vgimport
* vgimportclone
* vgmerge
* vgmknodes
* vgreduce
* vgremove
* vgrename
* vgs
* vgscan
* vgsplit

其中用到的：

* pvcreate _device_
* pvdisplay
* vgcreate _vgname_ _pvdevice_
* vgdisplay
* lvcreate -L _size_ -n _lvname_ _vgname_
* lvdisplay

创建好的逻辑卷可以通过/dev/_vgname_/_lvname_来访问，逻辑卷从使用上来讲和普通物理分区没什么区别，
都可以格式化（使用`mkfs.xxx`来格式化），挂载（使用`mount`），写到`/etc/fstab`中
（不过应该不可以通过uuid来挂载，见到的使用方式都是直接用路径来挂载的，不过lvm的路径也具备唯一性，
lvm的路径是由用户决定的，而不是由内核自动编号的，所以并没有什么影响）。

## 正常问题

* 逻辑卷的信息是存放在哪里的，猜测不是存放在一个系统的配置文件中的，而是存放在物理分区（PV）中。
* 逻辑卷的设备文件是何时被生成的，还是创建之后就一直在那里，
* lvm是如何运行的，操作系统如何判断是否有卷组，卷组的数据如何维护，
* lvm2-lvmetad.service以及相关的其他 systemd service是什么东西

## 蛋疼的问题

* 既然逻辑卷和一般分区用起来没什么区别，那可不可以将逻辑卷当作lvm的物理卷（pv）。如果可以当作PV，
那么可不可以直接加入到这个逻辑卷对应的卷组中去，虽然明显不科学，就是有点好奇允不允许。
