#spark-doc

## TODO第一

* spark单机最简部署文档整理

## TOC

### 学习记录

* [Apriori](APRIORI.md)
* [Spring framework](SPRING.md)
* [scala](SCALA.md)
* [Spark](SPARK.md)

### 读书笔记

* [Python for data analysis](PYTHON_FOR_DATA_ANALYSIS.md)

### MISC

* [LVM](LVM.md)
* [SPARK 开发环境，单机运行和伪分布式模式](SPARK-DEV-ENV.md)
* [认识的和不认识的单词或术语](WORDS.md)

## 简介

这是一个放各种乱七八糟的文档的仓库，因为接下来（自2015/06/18起）的一段时间重心都放在云计算，大数据，数据分析这么个领域上（没入门，用词不精确，以后修改），我也懒得把东西分散到各个地方，所以仓库名字叫spark-doc，但是内容涉及到各个方面。
