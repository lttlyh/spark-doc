# Spring 相关的笔记

## 数据访问代码与业务逻辑代码的编写的常见套路

// TODO: Fill in the blank

### DAO

### Entity

### Service

## Spring 的 XML 配置文件的编写以及 Spring 对其的解析过程

// TODO: Fill in the blank

## Spring MVC

### 简述

啊，就是一个mvc思想指导的web框架，一个处理http请求并构造响应的框架，
该框架提供的便捷功能有：

* 请求到处理方法的映射，强大的url路径处理，
* 通过注解的方式直接将请求中的数据注入到处理方法的制定参数中，
* restful服务的相关支持：路径参数，`@ResponseBody`，etc。
* 在不同的生命周期范围（Global, Session, Request, etc）中对Model的处理
  * `@ModelAttribute`, `@SessionAttribute`

总体流程：

1. 接受请求
2. 根据请求的Url和其他信息比如请求方法，请求体类型等等找到Handler
3. 调用Handler并获得一个ModelAndView
4. 根据ModelAndView或是请求或是其他什么的东西获得一个View
5. View根据ModelAndView渲染结果（Html页面，word文档，pdf文档，json，xml，etc）

细枝末节：

* 异常处理：内部错误（5xx），访问错误（4xx），etc。
* @ResponseBody和HttpMessageConverter
* 其他地方的类型转换，比如`@RequestParm`标注的一个参数会自动将数据转换为该参数的类型
* HttpEntity是Spring对一个HTTP请求或响应的包装
* `@ModelAttribute`, `@SessionAttribute`应用于方法和参数时的作用和区别

常见任务：

* rest 服务
* 表单处理
* session处理

### Components

主线流程上用到的组件：

* LocaleResolver
* ThemeResolver
* HandlerMapping
* HandlerAdapter
* HandlerExceptionResolver
* RequestToViewNameTranslator
* ViewResolver
* FlashMapManager

这些东西的默认配置参见`org.springframework.web.servlet.DispatcherServlet.properties`

需要注意的是当开启`@EnableWebMvc`或`<mvc:annotation-driven />`之后，会使用另一套HandlerXXX。

HandlerXXX：

// TODO: Fill in the blank


### Path Patterns


// TODO: Fill in the blank
