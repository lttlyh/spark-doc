# Python for Data Analysis

这是一本讲用python来处理数据的书，包含了对python的使用和数据分析的一些知识。

## Chapter 1


python是一个解释执行的语言，有一个交互式的命令行界面，所以使用起来相对java，c这些语言要便捷。书中讲到的python的好处是：python作为一个胶水将传统的代码，底层的计算性代码和python代码粘合起来。同时，python是一个general purpose编程语言，所以用python既可以做科学性，计算性编程，也可以做系统性，应用型编程，这样大家都用python，就不需要考虑怎么把两种语言集成在一起这么个问题了。但是，python也有缺点：执行速度慢，并发能力差，或者说并发编程困难。

几个python的包：

* numpy - 快速数组计算库
* pandas - 处理结构化数据
* matplotlib - 绘图
* ipython - 一个牛逼的shell
* scipy - 一些特殊领域比如积分，信号处理

## Chapter 2

处理数据的基本过程：

1. interacting with the outside world
2. preparation
3. transformation
4. modeling and computation
5. presentation

此处摘录_Data Mining - Concepts and Techniques_中对数据处理的章节的说法。额，原来看错了（打脸）。。。这本书上只集中讲了数据预处理的一些方式：包括_data cleaning_，_data integration_，_data reduction_。

稍微查了下网上的资料，[这里](http://www.51diaocha.com/20122/1330496836133940.shtml)认为数据分析的过程就是`收集->整理->分析`三个过程。这本书里面的分法比这个详细很多，将整理分为preparation和transformation，最后还特地提出一个presentation的过程。_Data Mining - Concepts and Techniques_所讲的数据预处理应该是处于数据准备和数据变换这么一个位置的东西。但是考虑到这本书讲的是数据挖掘，应该和数据分析还是侧重点有所不一样的，现在大概就了解这么多，还是要先将这些细节的一个一个东西都处理好之后才会看得明了一点。

### 看到的一些文章

* [北大老鸟三年数据分析深刻总结——致学弟学妹们](http://www.itongji.cn/article/041544062015.html)
