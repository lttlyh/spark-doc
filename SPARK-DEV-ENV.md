# Spark测试环境搭建

## Spark简介

Spark 是一个集群计算系统软件，是一个在集群环境中运行的分布式的，具有高容错性的，快速的计算框架。Spark为java，scala，python，R提供了高层API，所以可以使用java，scala，R编写Spark程序。同时，Spark 提供了一些高阶的工具用于特定的用途：其中Spark SQL用以做结构化数据处理，MLLib用以机器学习，GraphX用以进行图处理，Spark Streaming用以进行流式数据处理。

## Spark 核心概念

Spark的核心是RDD（Resilient Distributed Dataset），其实Spark是对RDD这么一个概念模型的实现。RDD全称是弹性分布式数据集，在[这篇论文](http://www.cs.berkeley.edu/~matei/papers/2011/tr_spark.pdf)中有关于其的详细介绍。简单来讲，RDD是一个只读的，分区的数据集合，其中可容纳的数据量不是单个机器能够承担的，所以RDD会将数据分区并存储到多台机器（集群）中去。真正有趣的是RDD的只读属性，他保证了其大部分时间都可以不实际存储数据，而是保留一个由其他RDD转化而来的线索，在最终要用到数据的时候在一次性运算。

Spark作为一个实现，其中又有很多的概念，在Spark的官方文档[_Cluster Mode Overview_](http://spark.apache.org/docs/latest/cluster-overview.html)中有Spark运行模型的图和对一些常见概念的解释。

在这里简单解释下几个概念。

Application

用户编写的Spark程序。Application跑起来之后分为两个部分：driver program和executors。

Driver Program

用户编写的Spark程序中main函数运行的进程。可能运行在集群中，也可能运行在用户电脑上，总之唯一非强制性的要求就是driver program需要离集群尽可能近，网络延迟尽可能小。Driver program也是创建并操作SparkContext的进程。

Executors

具体进行计算和数据存储（RDD）的进程。在工作节点（Worker node）上运行。一个executor进程独属于一个application，

job/stage/task

job是在对RDD进行一个action的时候被发起的一个并发任务，一个job由多个task组成，job可以分解成多组stage来对task分组，stage和stage之间有相互依赖关系。

## Spark安装

Spark作为一个针对集群环境设计的计算框架，在运行的时候需要一个集群管理器（Cluster Manager）来监管资源和进行调度操作。Spark支持三种集群管理器：Spark内置的Standalone，Apache Mesos，和Hadoop的YARN。此处为了方便采用Standalone的模式，因为这样就不需要对第三方的依赖，比如Hadoop。

运行Spark需要一个Java环境，所以在开始之前先往电脑上装一个JDK，这里选择比较稳定的1.7版本的Java环境。关于JDK的安装指南网上已经有很多了，此处不再赘述。注意将Java环境添加到系统或用户的PATH变量中去，因为Spark的运行脚本需要通过此来找到Java环境。

然后下载Spark，可以前往Spark的[官网](http://spark.apache.org)下载，当前已经发布到1.4.0版本。因为Apache的顶级项目在国内各大高校的都设有镜像站，所以也可以前往直接下载。在官网下载也会自动使用某一个镜像站点，这里给出Spark-1.4.0-hadoop2.6版本在阿里巴巴镜像的[下载地址](http://mirrors.aliyun.com/apache/spark/spark-1.4.0/spark-1.4.0-bin-hadoop2.6.tgz)。

本文中的Spark环境搭建在Fedora 22（Linux）上，未在Windows上成功搭建。根据报错信息和度娘的解释，应该是在Windows上运行需要一个独立的Hadoop发布包并且该发布包中有针对对应Windows平台编译的一些库。Hadoop-2.2可以在网上下到针对win7x64的本地库，也在csdn上看到针对Hadoop2.6的win8x64的本地库，不过那个要分，所以没下。

### Spark的目录结构

下面是Spark发布的包中的一些重要目录：

`bin & sbin`

可执行文件目录，其中存放的都是脚本文件。对spark的大部分操作都是通过存放在这里的脚本来进行的，比如启动主/副节点，开启交互shell，提交自己编写的Spark Application到集群环境中运行。`bin`目录下面的是普通的脚本，`sbin`下存放的是管理用的脚本，比如启动节点，关闭节点。但是`sbin`中存放的脚本都是shell脚本，在windows下无法运行。

对Spark的监管和控制基本上都是通过这些脚本和各个节点的Web UI界面来进行。

`conf`

存放各种各样的配置文件。

`lib`

这个目录存放的是Spark的核心jar包。

`work`

这个是Spark运行时候使用的临时目录。

### 几个重要的脚本

* bin/spark-submit 提交spark程序并运行的脚本
* bin/spark-shell 交互式的基于scala的spark shell
* bin/pyspark 交互式的基于python的spark shell
* sbin/{start,stop}-{master,slaves}.sh 启动停止主副节点的脚本

### web ui

Spark的master， worker， driver进程会提供一个通过浏览器访问的监管界面，从中可以查看到许多有用的信息。

### 运行Spark

Spark作为一个针对集群环境设计的计算框架，在生产环境下肯定是运行在集群中的，在集群中运算的时候会在主节点和工作节点这些节点上运行对应的服务程序（`org.apache.spark.deploy.master.Master`和`org.apache.spark.deploy.worker.Worker`）负责将节点联系起来，分配资源并将计算任务运行起来。但是在开发或者测试或是一个人玩玩的时候可以选择直接开单击模式，或是在自己的电脑上组建一个伪分布式的环境。下面先描述如何使用单机模式和部署伪分布式环境，然后讲如何运行自己编写的Spark应用。

#### 纯单机模式

纯单机模式使用起来是最简单的，只要设定Master URL为`local`,`local[k]`或`local[*]`即可。`local`表示只有一个工作线程，k表示运行k个工作线程，*表示当前电脑cpu有几个核心就运行几个线程。下面举几个例子：

```shell
cd ${SPARK_HOME}
MASTER=local bin/run-example SparkPi
```

这两条命令首先切换到spark的目录下（把`${SPARK_HOME}`替换为spark的目录），然后用单机模式运行Spark自带的演示程序SparkPi。该示例程序会打印一大堆log信息，在这堆log中间会有一句`Pi is roughly 3.14446`，需要仔细找找，具体数值可能会有所偏差。如果想要看得仔细一点，可以把第二个命令改为`MASTER=local bin/run-example SparkPi 2> /dev/null`。这两句话演示的就是一个单机执行Spark应用的过程，在此过程中并没有指定任何部署好的Spark集群的地址。不过这个运行的是Spark自带的演示程序。下面演示如何单机运行自己的Spark应用。

在运行之前，首先得有个Spark程序，这里就直接在Spark自带的`JavaSparkPi.java`上修改。

```java
// package语句可以去掉，免得构造太深的目录结构
    // .....
  public static void main(String[] args) throws Exception {
    SparkConf sparkConf = new SparkConf().setAppName("JavaSparkPi");
    // 在源文件中加入下面这一行
    sparkConf.setMaster("local[*]");
    JavaSparkContext jsc = new JavaSparkContext(sparkConf);
	// .....
  }
```
  
然后javac编译一下，java执行一下。需要注意的是，要把spark的lib目录下的spark-assembly-1.4.0-hadoop2.6.0.jar添加到classpath下。具体命令大概如下，路径会有所差异，替换成对应的spark目录即可。

```shell
javac -cp /home/liu/projects/cloud/hadoopspark/spark-1.4.0-bin-hadoop2.6/lib/spark-assembly-1.4.0-hadoop2.6.0.jar JavaSparkPi.java
java -cp /home/liu/projects/cloud/hadoopspark/spark-1.4.0-bin-hadoop2.6/lib/spark-assembly-1.4.0-hadoop2.6.0.jar JavaSparkPi 2> /dev/null
```

具体开发的时候最好将程序打包成jar包，必要的依赖也需要一并打入到jar包中，然后使用spark的bin目录下的spark-submit脚本来提交。

```shell
MASTER=local ${SPARK_HOME}/bin/spark-submit test.jar
```

不管是单机还是集群，spark程序的运行方式都是driver program和executors的模式。executors的启动与运行是由spark自己完成的，所以其中最主要问题就是要将driver program给跑起来，也就是我们编写的程序中的main函数。上述的运行spark程序的方式中最主要的是两种：通过spark-submit和直接运行main函数。通过run-example运行Spark自带的示例其实最终还是调用的spark-submit。通过spark-submit的一个好处是spark程序的jar包可以直接分发到各个节点上。因为spark是一个分布式计算框架，spark程序都需要通过将自身的至少负责计算的部分分发到各个计算节点上才能够完成计算任务，所以spark-submit这种行为可以为我们省下不少顾虑。同时，使用spark-submit可以通过设定`--deploy-mode cluster`选项将任务传送到集群中开启driver program，这样，在当前电脑与集群距离较远，延迟较高的时候，可以提高运行速度。而直接运行也有直接运行的好处，最直接的好处就是方便调试，因为程序是在本地开始运行的，可以直接从IDE中开始调试。

#### standalone伪分布式模式

单机运行虽然直接，但是与集群环境毕竟还是有所区别的，所以最好还是能够在本地创建一个伪集群环境，也就是master节点与worker节点都在本机上运行。

首先需要修改配置文件。

1. 复制conf/spark-env.sh.template到conf/spark-env.sh
2. 往conf/spark-env.sh添加如下几项
   * `SPARK_LOCAL_IP=127.0.0.1`
   * `SPARK_MASTER_IP=127.0.0.1`
3. 复制conf/spark-defaults.conf.template到conf/spark-defaults.conf
   * `spark.master  spark://127.0.0.1:7077`

然后运行master节点`sbin/start-master.sh`

然后运行worker节点`sbin/start-slaves.sh`

最后测试一下是否部署成功`MASTER=spark://127.0.0.1:7077 bin/run-example SparkPi`，如果运行失败，可以查看logs目录下的日志信息以了解具体在哪里处理问题。

单机模式与分布式模式的唯一区别就是要将master从local改为spark://hostname:port。当然此处所指的分布式模式是standalone模式，如果是运行在yarn上或是mesos上，master需要设置成对应的格式，并且可能还需要其他处理，详情可以从官方文档中找到。
